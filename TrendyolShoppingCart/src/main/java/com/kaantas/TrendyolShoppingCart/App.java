package com.kaantas.TrendyolShoppingCart;

import com.kaantas.TrendyolShoppingCart.model.Campaign;
import com.kaantas.TrendyolShoppingCart.model.Category;
import com.kaantas.TrendyolShoppingCart.model.Coupon;
import com.kaantas.TrendyolShoppingCart.model.Product;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.service.DeliveryCostCalculator;
import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public class App {

	public static void main(String[] args) {
		Category categoryFood = new Category("Food");
		Category categoryTech = new Category("Technology");
		Category categoryHome = new Category("Home");

		Product apple = new Product("Apple", 10, categoryFood);
		Product almond = new Product("Almond", 15, categoryFood);
		
		Product table = new Product("Table", 100, categoryHome);

		Product computer = new Product("Casper PC", 3000, categoryTech);

		ShoppingCart cart = ShoppingCart.getInstance();
		cart.addItem(computer, 2);
		cart.addItem(apple, 5); // 50
		cart.addItem(almond, 10); // 150
		cart.addItem(table, 2);

		Campaign campaign1 = new Campaign(categoryFood, 20, 5, DiscountType.Rate);
		Campaign campaign2 = new Campaign(categoryTech, 30, 1, DiscountType.Amount);
		cart.applyDiscounts(campaign1, campaign2);

		Coupon coupon = new Coupon(100, 10, DiscountType.Rate);
		cart.applyCoupon(coupon);

		DeliveryCostCalculator deliveryCostCalculator = new DeliveryCostCalculator(2, 2);
		double deliveryCost = deliveryCostCalculator.calculateFor(cart);
		cart.setDeliveryCost(deliveryCost);

		cart.print();
	}
}
