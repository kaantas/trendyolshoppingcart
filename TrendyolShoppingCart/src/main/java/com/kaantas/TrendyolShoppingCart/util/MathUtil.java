package com.kaantas.TrendyolShoppingCart.util;

import com.kaantas.TrendyolShoppingCart.model.Discount;

public class MathUtil {
	
	public static double getAmountByRate(double amount, double rate) {
		return (amount * rate) / 100;
	}

	public static double calculateDiscountByType(double value, Discount discount) {
		double discountValue = 0;
		if (DiscountType.Rate == discount.getDiscountType()) {
			discountValue = getAmountByRate(value, discount.getDiscountAmount());
		} else {
			discountValue = discount.getDiscountAmount();
		}
		return discountValue;
	}
}
