package com.kaantas.TrendyolShoppingCart.intf;

//Strategy
public interface ICalculateDiscount {
	void calculateDiscount();
}
