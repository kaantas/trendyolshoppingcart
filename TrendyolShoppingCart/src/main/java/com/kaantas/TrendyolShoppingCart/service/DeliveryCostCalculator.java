package com.kaantas.TrendyolShoppingCart.service;

import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;

public class DeliveryCostCalculator {
	private double costPerDelivery;
	private double costPerProduct;
	private static final double fixedCost = 2.99;
	
	public DeliveryCostCalculator(double costPerDelivery, double costPerProduct) {
		this.costPerDelivery = costPerDelivery;
		this.costPerProduct = costPerProduct;
	}
	
	public double calculateFor(ShoppingCart cart) {
		return (costPerDelivery * cart.getNumOfDeliveries()) + (costPerProduct * cart.getNumOfProducts()) + fixedCost;
	}
	
}
