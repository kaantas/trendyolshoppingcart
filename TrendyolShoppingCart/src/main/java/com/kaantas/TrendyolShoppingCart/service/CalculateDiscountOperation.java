package com.kaantas.TrendyolShoppingCart.service;

import com.kaantas.TrendyolShoppingCart.intf.ICalculateDiscount;

public class CalculateDiscountOperation {
	private ICalculateDiscount calculateDiscount;
	
	public CalculateDiscountOperation(ICalculateDiscount calculateDiscount) {
		this.calculateDiscount = calculateDiscount;
	}
	
	public void calculateDiscount() {
		this.calculateDiscount.calculateDiscount();
	}
}
