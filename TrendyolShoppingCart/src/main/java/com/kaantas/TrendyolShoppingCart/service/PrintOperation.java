package com.kaantas.TrendyolShoppingCart.service;

import com.kaantas.TrendyolShoppingCart.intf.IPrintType;

public class PrintOperation {
	private IPrintType printType;
	
	public PrintOperation(IPrintType printType) {
		this.printType = printType;
	}
	
	public void printByType() {
		this.printType.printByType();
	}
}
