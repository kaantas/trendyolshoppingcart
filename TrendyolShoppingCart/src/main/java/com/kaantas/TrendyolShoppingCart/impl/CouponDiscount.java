package com.kaantas.TrendyolShoppingCart.impl;

import com.kaantas.TrendyolShoppingCart.intf.ICalculateDiscount;
import com.kaantas.TrendyolShoppingCart.model.Coupon;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.util.MathUtil;

//concrate Strategy
public class CouponDiscount implements ICalculateDiscount {

	private Coupon coupon;
	private ShoppingCart shoppingCart;

	public CouponDiscount(Coupon coupon, ShoppingCart shoppingCart) {
		this.coupon = coupon;
		this.shoppingCart = shoppingCart;
	}

	@Override
	public void calculateDiscount() {
		boolean isCouponApplied = shoppingCart.isCouponApplied();
		double totalAmount = shoppingCart.getTotalAmount();

		if (!isCouponApplied && totalAmount > coupon.getMinAmount()) {
			double discountValue = MathUtil.calculateDiscountByType(totalAmount, coupon);
			shoppingCart.setCouponDiscount(discountValue);
			shoppingCart.setCouponApplied(true);
			shoppingCart.setTotalAmount(totalAmount - discountValue);
		} else if (isCouponApplied) {
			throw new RuntimeException("Daha önce sepete kupon eklenmiştir. Bir daha kupon eklenemez.");
		} else if (totalAmount < coupon.getMinAmount()) {
			throw new RuntimeException(
					"Sepetinizin toplam tutarı kuponun minimum tutarından küçüktür. Kupon eklenemez.");
		}
	}
}
