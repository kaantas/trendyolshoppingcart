package com.kaantas.TrendyolShoppingCart.impl;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import com.kaantas.TrendyolShoppingCart.intf.IPrintType;
import com.kaantas.TrendyolShoppingCart.model.Campaign;
import com.kaantas.TrendyolShoppingCart.model.Product;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;

public class PrintToConsole implements IPrintType {

	private ShoppingCart shoppingCart;
	private TreeMap<String, HashMap<Product, Integer>> cartMapForPrint;

	public PrintToConsole(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	@Override
	public void printByType() {
		fillPrintMap();
		System.out.println(
				"-----------------------------------------------------------------------------------------------");
		System.out.printf("%10s %15s %15s %15s %15s %20s", "Category", "Product", "Quantity", "Unit Price",
				"Total Price", "Total Discount");
		System.out.println();
		System.out.println(
				"-----------------------------------------------------------------------------------------------");
		Set<Entry<String, HashMap<Product, Integer>>> categoryMap = cartMapForPrint.entrySet();
		Campaign selectedCampaign = shoppingCart.getCampaignDiscount();
		for (Entry<String, HashMap<Product, Integer>> entry : categoryMap) {
			System.out.format("%10s", entry.getKey());
			if (selectedCampaign.getCategoryOfCampaign().getTitle().equals(entry.getKey())) {
				System.out.format("%85s", shoppingCart.getMaxCampaignDiscountAmount() + " TL");
			}

			System.out.println();
			Set<Entry<Product, Integer>> lstOfKey = entry.getValue().entrySet();
			for (Entry<Product, Integer> entry2 : lstOfKey) {
				System.out.format("%25s %15d %15s %15s", entry2.getKey().getTitle(), entry2.getValue(),
						entry2.getKey().getPrice() + " TL", entry2.getKey().getPrice() * entry2.getValue() + " TL");
				System.out.println();

			}
			System.out.println(
					"-----------------------------------------------------------------------------------------------");

		}
		System.out.printf("%95s", "Coupon Discount");
		System.out.println();
		System.out.format("%95s", shoppingCart.getCouponDiscount() + " TL");
		System.out.println();
		System.out.println(
				"-----------------------------------------------------------------------------------------------");

		System.out.printf("%95s", "Total Amount");
		System.out.println();
		System.out.format("%95s", shoppingCart.getTotalAmount() + " TL");
		System.out.println();
		System.out.println(
				"-----------------------------------------------------------------------------------------------");
		
		System.out.printf("%95s", "Delivery Amount");
		System.out.println();
		System.out.format("%95s", new DecimalFormat(".##").format(shoppingCart.getDeliveryCost()).replaceAll(",", ".") + " TL");
		System.out.println();
		System.out.println(
				"-----------------------------------------------------------------------------------------------");
		
		System.out.printf("%95s", "Total Amount with Delivery");
		System.out.println();
		System.out.format("%95s", (shoppingCart.getDeliveryCost() + shoppingCart.getTotalAmount()) + " TL");
		System.out.println();
		System.out.println(
				"-----------------------------------------------------------------------------------------------");
	}

	private void fillPrintMap() {
		if (cartMapForPrint == null) {
			cartMapForPrint = new TreeMap<>();
		}
		Set<Entry<Product, Integer>> set = shoppingCart.getItemsOfCart().entrySet();
		for (Entry<Product, Integer> entry : set) {
			if (cartMapForPrint.containsKey(entry.getKey().getCategory().getTitle())) {
				HashMap<Product, Integer> lstProduct = cartMapForPrint.get(entry.getKey().getCategory().getTitle());
				lstProduct.put(entry.getKey(), entry.getValue());
			} else {
				HashMap<Product, Integer> lst = new HashMap<Product, Integer>();
				lst.put(entry.getKey(), entry.getValue());
				cartMapForPrint.put(entry.getKey().getCategory().getTitle(), lst);
			}
		}
	}
}
