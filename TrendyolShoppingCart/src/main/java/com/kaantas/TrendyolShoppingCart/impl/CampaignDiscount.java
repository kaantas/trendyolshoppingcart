package com.kaantas.TrendyolShoppingCart.impl;

import java.util.Arrays;
import java.util.List;

import com.kaantas.TrendyolShoppingCart.intf.ICalculateDiscount;
import com.kaantas.TrendyolShoppingCart.model.Campaign;
import com.kaantas.TrendyolShoppingCart.model.Category;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.util.MathUtil;

//concrete Strategy
public class CampaignDiscount implements ICalculateDiscount {

	private Campaign[] campaigns;
	private ShoppingCart shoppingCart;

	public CampaignDiscount(Campaign[] campaigns, ShoppingCart shoppingCart) {
		setCampaigns(campaigns);
		setShoppingCart(shoppingCart);
	}

	public Campaign[] getCampaigns() {
		return campaigns;
	}

	public void setCampaigns(Campaign[] campaigns) {
		this.campaigns = campaigns;
	}

	public ShoppingCart getShoppingCart() {
		return shoppingCart;
	}

	public void setShoppingCart(ShoppingCart shoppingCart) {
		this.shoppingCart = shoppingCart;
	}

	@Override
	public void calculateDiscount() {
		boolean isCouponApplied = shoppingCart.isCouponApplied();
		boolean isCampaignDiscountApplied = shoppingCart.isCampaignDiscountApplied();
		double totalAmount = shoppingCart.getTotalAmount();
		double maxDiscount = 0;
		Campaign selectedCampaign = null;
		List<Campaign> campaignList = Arrays.asList(campaigns);

		if (isCouponApplied) {
			throw new RuntimeException("Önce kupon eklenemez. Obje yaratma akışında yanlışlık var.");
		}
		if (!isCampaignDiscountApplied) {
			for (Campaign campaign : campaignList) {
				int quantityOfCategory = getTotalQuantityOfProductsByCategory(campaign.getCategoryOfCampaign());
				if (quantityOfCategory > campaign.getNumOfItems()) {
					double discountValue = MathUtil.calculateDiscountByType(
							getTotalAmountOfProductsByCategory(campaign.getCategoryOfCampaign()), campaign);
					if (discountValue > maxDiscount) {
						maxDiscount = discountValue;
						selectedCampaign = campaign;
					}
					isCampaignDiscountApplied = true;
					shoppingCart.setCampaignDiscountApplied(isCampaignDiscountApplied);
				}
			}
		} else {
			throw new RuntimeException("ApplyDiscounts zaten bir kere kullanılmış.");
		}

		shoppingCart.setCampaignDiscount(selectedCampaign);
		shoppingCart.setMaxCampaignDiscountAmount(maxDiscount);
		shoppingCart.setTotalAmount(totalAmount - maxDiscount);
	}

	private double getTotalAmountOfProductsByCategory(Category category) {
		return shoppingCart.getItemsOfCart().entrySet().stream()
				.filter(entry -> category.getTitle().equals(entry.getKey().getCategory().getTitle()))
				.mapToDouble(entry -> (entry.getKey().getPrice() * entry.getValue())).sum();
	}

	private int getTotalQuantityOfProductsByCategory(Category category) {
		return shoppingCart.getItemsOfCart().entrySet().stream()
				.filter(entry -> category.getTitle().equals(entry.getKey().getCategory().getTitle()))
				.mapToInt(entry -> entry.getValue()).sum();
	}
}
