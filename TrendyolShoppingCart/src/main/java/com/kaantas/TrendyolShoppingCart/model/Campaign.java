package com.kaantas.TrendyolShoppingCart.model;

import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public class Campaign extends Discount {
	
	private Category category;
	private int numOfItems;
	
	public Campaign(Category category, double discount, int numOfItems, DiscountType discountType) {
		super(discount, discountType);
		setCategoryOfCampaign(category);
		setNumOfItems(numOfItems);
	}
	
	public Category getCategoryOfCampaign() {
		return category;
	}
	
	public void setCategoryOfCampaign(Category category) {
		this.category = category;
	}

	public int getNumOfItems() {
		return numOfItems;
	}

	public void setNumOfItems(int numOfItems) {
		try {
			if (numOfItems < 0) {
				throw new Exception("Item sayısı negatif değer olamaz: " + numOfItems);
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		this.numOfItems = numOfItems;
	}
}
