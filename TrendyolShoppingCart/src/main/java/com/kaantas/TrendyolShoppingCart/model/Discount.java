package com.kaantas.TrendyolShoppingCart.model;

import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public abstract class Discount {

	private double discountAmount;
	private DiscountType discountType;

	public Discount() {
	}

	public Discount(double discountAmount, DiscountType discountType) {
		if (discountAmount < 0) {
			throw new RuntimeException("Fiyat negatif değer olamaz: " + discountAmount);
		}

		this.setDiscountAmount(discountAmount);
		this.setDiscountType(discountType);
	}

	public double getDiscountAmount() {
		return discountAmount;
	}

	public void setDiscountAmount(double discountAmount) {
		this.discountAmount = discountAmount;
	}

	public DiscountType getDiscountType() {
		return discountType;
	}

	public void setDiscountType(DiscountType discountType) {
		this.discountType = discountType;
	}
}
