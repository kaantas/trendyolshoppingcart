package com.kaantas.TrendyolShoppingCart.model;

import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public class Coupon extends Discount {

	private double minAmount;

	public Coupon(double minAmount, double discount, DiscountType discountType) {
		super(discount, discountType);

		if (minAmount < 0) {
			throw new RuntimeException("Fiyat negatif değer olamaz: " + minAmount);
		}
		this.minAmount = minAmount;
	}

	public double getMinAmount() {
		return minAmount;
	}
}
