package com.kaantas.TrendyolShoppingCart.model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.kaantas.TrendyolShoppingCart.impl.CampaignDiscount;
import com.kaantas.TrendyolShoppingCart.impl.CouponDiscount;
import com.kaantas.TrendyolShoppingCart.impl.PrintToConsole;
import com.kaantas.TrendyolShoppingCart.service.CalculateDiscountOperation;
import com.kaantas.TrendyolShoppingCart.service.PrintOperation;

public class ShoppingCart {
	// Proje yaşam döngüsü boyunca yalnızca 1 tane shoppingCart olacağı için
	// Singleton oluşturuldu.
	private static ShoppingCart shoppingCart = null;
	private HashMap<Product, Integer> itemsOfCart = null;
	private double totalAmount;
	private boolean isCouponApplied;
	private boolean isCampaignDiscountApplied;
	private double deliveryCost;
	private double couponDiscount;
	private Campaign campaignDiscount;
	private double maxCampaignDiscountAmount;

	private ShoppingCart() {
		itemsOfCart = new HashMap<Product, Integer>();
		totalAmount = 0;
	}

	public static ShoppingCart getInstance() {
		if (shoppingCart == null) {
			shoppingCart = new ShoppingCart();
		}
		return shoppingCart;
	}

	public double getDeliveryCost() {
		return deliveryCost;
	}

	public void setDeliveryCost(double deliveryCost) {
		this.deliveryCost = deliveryCost;
	}

	public double getCouponDiscount() {
		return couponDiscount;
	}

	public void setCouponDiscount(double couponDiscount) {
		this.couponDiscount = couponDiscount;
	}

	public Campaign getCampaignDiscount() {
		return campaignDiscount;
	}

	public void setCampaignDiscount(Campaign campaignDiscount) {
		this.campaignDiscount = campaignDiscount;
	}

	public double getMaxCampaignDiscountAmount() {
		return maxCampaignDiscountAmount;
	}

	public void setMaxCampaignDiscountAmount(double maxCampaignDiscountAmount) {
		this.maxCampaignDiscountAmount = maxCampaignDiscountAmount;
	}

	public void setCouponApplied(boolean isCouponApplied) {
		this.isCouponApplied = isCouponApplied;
	}

	public void setCampaignDiscountApplied(boolean isCampaignDiscountApplied) {
		this.isCampaignDiscountApplied = isCampaignDiscountApplied;
	}

	public boolean isCouponApplied() {
		return isCouponApplied;
	}

	public boolean isCampaignDiscountApplied() {
		return isCampaignDiscountApplied;
	}

	public HashMap<Product, Integer> getItemsOfCart() {
		return itemsOfCart;
	}

	public void setItemsOfCart(HashMap<Product, Integer> itemsOfCart) {
		this.itemsOfCart = itemsOfCart;
	}

	public double getTotalAmount() {
		return totalAmount;
	}

	public void setTotalAmount(double totalAmount) {
		this.totalAmount = totalAmount;
	}

	public void addItem(Product product, Integer quantity) {
		if (quantity < 0) {
			throw new RuntimeException("Miktar negatif değer olamaz: " + quantity);
		}

		int quantityOfCart = 0;
		if (itemsOfCart.containsKey(product)) {
			quantityOfCart = itemsOfCart.get(product);
		}
		quantityOfCart = quantityOfCart + quantity;
		itemsOfCart.put(product, quantityOfCart);

		addAmount(product, quantity);

	}

	private void addAmount(Product product, Integer quantity) {
		totalAmount = totalAmount + product.getPrice() * quantity;
		setTotalAmount(totalAmount);
	}

	public void removeItem(Product product) {
		if (!itemsOfCart.containsKey(product)) {
			throw new RuntimeException("Ürün sepete eklenmemiş. Silinemez. " + product.getTitle());
		}

		int quantityOfCart = itemsOfCart.get(product);
		itemsOfCart.remove(product);
		reduceAmount(product, quantityOfCart);
	}

	private void reduceAmount(Product product, Integer quantity) {
		totalAmount = totalAmount - product.getPrice() * quantity;
		setTotalAmount(totalAmount);
	}

	public void clearCart() {
		if (itemsOfCart == null || itemsOfCart.size() == 0) {
			throw new RuntimeException("Sepetiniz zaten boş.");
		}

		itemsOfCart.clear();
		setTotalAmount(0);
		shoppingCart.setCouponApplied(false);
		shoppingCart.setCampaignDiscountApplied(false);
	}

	public void applyDiscounts(Campaign... campaigns) {
		CalculateDiscountOperation calculateDiscountOperation = new CalculateDiscountOperation(
				new CampaignDiscount(campaigns, shoppingCart));
		calculateDiscountOperation.calculateDiscount();
	}

	public void applyCoupon(Coupon coupon) {
		CalculateDiscountOperation calculateDiscountOperation = new CalculateDiscountOperation(
				new CouponDiscount(coupon, shoppingCart));
		calculateDiscountOperation.calculateDiscount();
	}

	public Set<String> getDistinctCategories() {
		Set<String> distinctCategories = new HashSet<String>();
		itemsOfCart.entrySet().stream()
				.forEach(entry -> distinctCategories.add(entry.getKey().getCategory().getTitle()));
		return distinctCategories;
	}

	public int getNumOfDeliveries() {
		return getDistinctCategories().size();
	}

	public int getNumOfProducts() {
		Set<Product> distinctProducts = new HashSet<Product>();
		itemsOfCart.entrySet().stream().forEach(entry -> distinctProducts.add(entry.getKey()));
		return distinctProducts.size();
	}

	public void print() {
		PrintOperation printOperation = new PrintOperation(new PrintToConsole(shoppingCart));
		printOperation.printByType();
	}

	public double getTotalAmountAfterDiscounts() {
		return getTotalAmount();
	}
}
