package com.kaantas.TrendyolShoppingCart.model;

public class Product {

	private String title;
	private double price;
	private Category category;

	public Product(String title, double price, Category category) {
		this.title = title;
		this.category = category;

		if (price < 0) {
			throw new RuntimeException("Fiyat negatif değer olamaz: " + price);
		}
		this.price = price;
	}

	public String getTitle() {
		return title;
	}

	public double getPrice() {
		return price;
	}

	public Category getCategory() {
		return category;
	}
}
