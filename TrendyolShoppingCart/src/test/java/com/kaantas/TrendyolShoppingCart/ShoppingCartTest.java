package com.kaantas.TrendyolShoppingCart;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.HashMap;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.kaantas.TrendyolShoppingCart.model.Campaign;
import com.kaantas.TrendyolShoppingCart.model.Category;
import com.kaantas.TrendyolShoppingCart.model.Coupon;
import com.kaantas.TrendyolShoppingCart.model.Product;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public class ShoppingCartTest {

	private ShoppingCart shoppingCart;
	private Category categoryFood;
	private Category categoryTech;
	private Product productApple;
	private Product productBanana;
	private Product productComputer;

	@Before
	public void setUp() {
		shoppingCart = ShoppingCart.getInstance();
		categoryFood = new Category("Food");
		categoryFood = new Category("Food");
		categoryTech = new Category("Technology");
		productApple = new Product("apple", 5, categoryFood);
		productBanana = new Product("banana", 10, categoryFood);
		productComputer = new Product("Casper PC", 3000, categoryTech);
		shoppingCart.addItem(productApple, 6);
		shoppingCart.addItem(productBanana, 2);
		shoppingCart.addItem(productComputer, 1);
		
	}

	@After
	public void after() {
		shoppingCart.getItemsOfCart().clear();
		shoppingCart.setTotalAmount(0);
		shoppingCart.setCouponApplied(false);
		shoppingCart.setCampaignDiscountApplied(false);
	}

	@Test
	public void testAddItem() {
		// apple x 6, banana x 1, Casper PC x 1, iPhone x 1
		Product productIphone = new Product("iPhone", 5000, categoryTech);
		shoppingCart.addItem(productIphone, 1);
		
		HashMap<Product, Integer> itemsOfCart = shoppingCart.getItemsOfCart();
		assertTrue(itemsOfCart.containsKey(productBanana));
		assertTrue(itemsOfCart.containsKey(productIphone));

		assertEquals(new Integer(6), itemsOfCart.get(productApple));
		assertEquals(new Integer(1), itemsOfCart.get(productIphone));
	}

	@Test
	public void testAddAmountAfterAddItem() {
		// apple x 6, banana x 1, Casper PC x 1 = 3050 TL
		Product productIphone = new Product("iPhone", 5000, categoryTech);
		shoppingCart.addItem(productIphone, 1);
		// apple x 6, banana x 1, Casper PC x 1, iPhone x 1 = 8050 TL
		assertEquals(new Double(8050), new Double(shoppingCart.getTotalAmount()));
	}

	@Test
	public void testRemoveItem() {
		// apple x 6, banana x 1, Casper PC x 1
		shoppingCart.removeItem(productApple);
		// banana x 1, Casper PC x 1
		HashMap<Product, Integer> itemsOfCart = shoppingCart.getItemsOfCart();

		assertFalse(itemsOfCart.containsKey(productApple));
		assertNull(itemsOfCart.get(productApple));
	}

	@Test
	public void testReduceAmountAfterRemoveItem() {
		// apple x 6, banana x 1, Casper PC x 1 = 3050 TL
		shoppingCart.removeItem(productApple);
		// banana x 1, Casper PC x 1 = 3020 TL
		assertEquals(new Double(3020), new Double(shoppingCart.getTotalAmount()));
	}

	@Test
	public void testClearCart() {
		shoppingCart.clearCart();
		assertEquals(0, shoppingCart.getItemsOfCart().size());
	}

	@Test
	public void testClearAmountAfterClearCart() {
		shoppingCart.clearCart();
		assertEquals(new Double(0), new Double(shoppingCart.getTotalAmount()));
	}

	@Test
	public void testApplyDiscounts() {
		Campaign campaign1 = new Campaign(categoryFood, 20, 5, DiscountType.Rate);
		Campaign campaign2 = new Campaign(categoryTech, 30, 1, DiscountType.Amount);

		// 10TL discount for food category because of apple with 6 quantity
		shoppingCart.applyDiscounts(campaign1, campaign2);

		// total amount 3050 - 10 = 3040 TL
		assertEquals(new Double(3040), new Double(shoppingCart.getTotalAmount()));
	}

	@Test
	public void testApplyCouponWithoutCampaign() {
		// 3050 TL total > %10 Discount = 305TL > 2745 TL
		Coupon coupon = new Coupon(100, 10, DiscountType.Rate);
		shoppingCart.applyCoupon(coupon);

		assertEquals(new Double(2745), new Double(shoppingCart.getTotalAmount()));
	}

	@Test
	public void testApplyCouponWithCampaign() {
		Campaign campaign1 = new Campaign(categoryFood, 20, 5, DiscountType.Rate);
		Campaign campaign2 = new Campaign(categoryTech, 30, 1, DiscountType.Amount);

		// 10TL discount for food category because of apple with 6 quantity
		// Total amount after discount of campaign is 3040 TL
		shoppingCart.applyDiscounts(campaign1, campaign2);

		// 3040 TL total > %10 Discount = 304 TL
		// Total amount after discount of coupon is 2736 TL
		Coupon coupon = new Coupon(100, 10, DiscountType.Rate);
		shoppingCart.applyCoupon(coupon);

		assertEquals(new Double(2736), new Double(shoppingCart.getTotalAmount()));
	}

	@Test
	public void testGetDistinctCategories() {
		// Food, Technology
		Set<String> distinctCategories = shoppingCart.getDistinctCategories();
		assertTrue(distinctCategories.contains("Food"));
		assertTrue(distinctCategories.contains("Technology"));
		assertFalse(distinctCategories.contains("Home"));
	}

	@Test
	public void testGetNumOfDeliveries() {
		Set<String> distinctCategories = shoppingCart.getDistinctCategories();
		// Food, Technology
		assertEquals(2, distinctCategories.size());
	}

	@Test
	public void testGetNumOfProducts() {
		// apple, banana, computer
		assertEquals(3, shoppingCart.getNumOfProducts());
	}
	
	//TODO: testPrint
	public void testPrint() {
		// apple, banana, computer
		//assertEquals(3, shoppingCart.getNumOfProducts());
	}
}
