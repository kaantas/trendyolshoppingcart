package com.kaantas.TrendyolShoppingCart;

import static org.junit.Assert.assertEquals;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.kaantas.TrendyolShoppingCart.impl.CampaignDiscount;
import com.kaantas.TrendyolShoppingCart.intf.ICalculateDiscount;
import com.kaantas.TrendyolShoppingCart.model.Campaign;
import com.kaantas.TrendyolShoppingCart.model.Category;
import com.kaantas.TrendyolShoppingCart.model.Product;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public class CampaignDiscountOperationTest {

	private ICalculateDiscount campaignDiscount;
	private ShoppingCart cart;
	private Category categoryFood, categoryTech;

	@Before
	public void setup() {
		categoryFood = new Category("Food");
		categoryTech = new Category("Technology");
		Product apple = new Product("Apple", 10, categoryFood);
		Product almond = new Product("Almond", 15, categoryFood);
		Product computer = new Product("Casper PC", 3000, categoryTech);
		cart = ShoppingCart.getInstance();
		cart.addItem(computer, 1); // 3000
		cart.addItem(apple, 5); // 50
		cart.addItem(almond, 10); // 150
		Campaign campaign1 = new Campaign(categoryFood, 20, 5, DiscountType.Rate);
		Campaign campaign2 = new Campaign(categoryTech, 30, 1, DiscountType.Amount);
		Campaign[] campaigns = { campaign1, campaign2 };
		campaignDiscount = new CampaignDiscount(campaigns, cart);
	}

	@After
	public void after() {
		cart.getItemsOfCart().clear();
		cart.setTotalAmount(0);
		cart.setCouponApplied(false);
		cart.setCampaignDiscountApplied(false);
	}

	@Test
	public void testCalculateDiscount() {
		// campaign2 is not applied because of quantity = 1
		// campaign1 is applied, total amount of food is 200 TL and discount is 40 TL by
		// Rate.
		campaignDiscount.calculateDiscount();
		// max Discount = 40 TL
		assertEquals(new Double(40), new Double(cart.getMaxCampaignDiscountAmount()));
		// total amount after discount = 3200 - 40 = 3160 TL
		assertEquals(new Double(3160), new Double(cart.getTotalAmount()));
	}

	@Test
	public void testGetTotalAmountOfProductsByCategory() throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method method = CampaignDiscount.class.getDeclaredMethod("getTotalAmountOfProductsByCategory", Category.class);
		method.setAccessible(true);
		double totalAmountForCategoryFood = (double) method.invoke(campaignDiscount, categoryFood);
		// apple = 50TL , almond = 150 TL = total 200TL
		assertEquals(new Double(200), new Double(totalAmountForCategoryFood));
	}

	@Test
	public void testGetTotalQuantityOfProductsByCategory() throws NoSuchMethodException, SecurityException,
			IllegalAccessException, IllegalArgumentException, InvocationTargetException {
		Method method = CampaignDiscount.class.getDeclaredMethod("getTotalQuantityOfProductsByCategory",
				Category.class);
		method.setAccessible(true);
		int totalAmountForCategoryFood = (int) method.invoke(campaignDiscount, categoryFood);
		// apple x 5 , almond x 10 = total 15
		assertEquals(new Integer(15), new Integer(totalAmountForCategoryFood));
	}
}
