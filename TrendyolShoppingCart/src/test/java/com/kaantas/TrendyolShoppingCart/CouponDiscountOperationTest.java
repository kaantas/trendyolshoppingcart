package com.kaantas.TrendyolShoppingCart;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.kaantas.TrendyolShoppingCart.impl.CouponDiscount;
import com.kaantas.TrendyolShoppingCart.intf.ICalculateDiscount;
import com.kaantas.TrendyolShoppingCart.model.Category;
import com.kaantas.TrendyolShoppingCart.model.Coupon;
import com.kaantas.TrendyolShoppingCart.model.Product;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.util.DiscountType;

public class CouponDiscountOperationTest {

	private Coupon coupon;
	private ShoppingCart cart;
	private Category categoryFood;
	private ICalculateDiscount couponDiscount;

	@Before
	public void setup() {
		categoryFood = new Category("Food");
		Product apple = new Product("Apple", 10, categoryFood);
		Product almond = new Product("Almond", 15, categoryFood);
		cart = ShoppingCart.getInstance();
		cart.addItem(apple, 5); // 50
		cart.addItem(almond, 10); // 150

	    coupon = new Coupon(100, 10, DiscountType.Rate);
		couponDiscount = new CouponDiscount(coupon, cart);
	}

	@After
	public void after() {
		cart.getItemsOfCart().clear();
		cart.setTotalAmount(0);
		cart.setCouponApplied(false);
		cart.setCampaignDiscountApplied(false);
	}

	@Test
	public void testCalculateDiscount() {
		// minAmount = 100TL
		// cartAmount = 200TL
		// totalAmount = 200-(200*10)/100 = 180 TL
		couponDiscount.calculateDiscount();
		// coupon Discount = 20 TL
		assertEquals(new Double(20), new Double(cart.getCouponDiscount()));
		// total amount after discount = 180 TL
		assertEquals(new Double(180), new Double(cart.getTotalAmount()));
	}
}
