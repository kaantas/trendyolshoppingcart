package com.kaantas.TrendyolShoppingCart;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import com.kaantas.TrendyolShoppingCart.model.Category;
import com.kaantas.TrendyolShoppingCart.model.Product;
import com.kaantas.TrendyolShoppingCart.model.ShoppingCart;
import com.kaantas.TrendyolShoppingCart.service.DeliveryCostCalculator;

public class DeliveryCostCalculatorTest {

	private DeliveryCostCalculator deliveryCostCalculator;
	private ShoppingCart shoppingCart;

	@Before
	public void setup() {
		deliveryCostCalculator = new DeliveryCostCalculator(1, 1);
		Category categoryFood = new Category("Food");
		Category categoryTech = new Category("Technology");
        Product apple = new Product("Apple", 10, categoryFood);
        Product almond = new Product("Almond", 15, categoryFood);
        Product computer = new Product("Casper PC", 3000, categoryTech);
        shoppingCart = ShoppingCart.getInstance();
        shoppingCart.addItem(apple, 5);
        shoppingCart.addItem(almond, 3);
        shoppingCart.addItem(computer, 2);
	}

	@Test
	public void testCalculateFor() {
        //distinct Categories = 2
        //distinct Products = 3
        // expected = 1*2 + 1*3 + 2.99 = 7.99TL
        assertEquals(new Double(7.99), new Double(deliveryCostCalculator.calculateFor(shoppingCart)));
	}
}
